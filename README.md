# translate-subtitles

[![Build Status](https://storage.googleapis.com/derat-build-badges/0010cfc4-cc4b-47b9-8e65-e0b39f5f947f.svg)](https://storage.googleapis.com/derat-build-badges/0010cfc4-cc4b-47b9-8e65-e0b39f5f947f.html)

This is a tiny command-line program program for translating [SubRip] `.srt`
subtitle files to different languages using the [Google Cloud Translation API].

<img src="https://www.erat.org/programming/subtitle_viewer-1920.png"
     width="640" border="1" alt="[screenshot]">

[SubRip]: https://en.wikipedia.org/wiki/SubRip
[Google Cloud Translation API]: https://cloud.google.com/translate

## Installation

[Install Go] and run `go install` in this repository.

[Install Go]: https://go.dev/doc/install

## Usage

The program reads subtitle data from a file or from stdin and writes it with the
added translations to a file or to stdout. It has a few flags:

```
Usage: translate-subtitles [flag]... <in.srt> <out.srt>
Translate a SubRip .srt subtitle file using GCP.
Reads from stdin and writes to stdout if no args are supplied.

  -dst-lang string
    	Target language (default "zh-CN")
  -max-lines int
    	Maximum number of lines to translate
  -project-id string
    	GCP project ID
  -src-charset string
    	Source charset if not ASCII or UTF-8, e.g. "latin1"
  -src-lang string
    	Source language (default "en-US")
```

At the very least, you'll need to enable the Cloud Translation API for a GCP
project and then pass that project's ID via the `-project-id` flag.

The project ID can also be specified via a `TRANSLATE_SUBTITLES_PROJECT_ID`
environment variable.

The `-src-charset` flag accepts official IANA character set names as listed at
<https://www.iana.org/assignments/character-sets/character-sets.xhtml>.

## Other notes

The [subtitle-viewer] web app may be useful for viewing translated subtitles.

[OpenCC] can be used to convert subtitle files between Traditional and
Simplified Chinese:

```
opencc -c /usr/share/opencc/t2s.json -i <traditional.srt> -o <simplified.srt>
```

If the original file uses the UTF-16 character encoding, you may need to convert
it to UTF-8 first using [iconv]:

```
iconv -f utf-16 -t utf-8 subs.srt >subs-utf8.srt
```

[subtitle-viewer]: https://codeberg.org/derat/subtitle-viewer
[OpenCC]: https://github.com/BYVoid/OpenCC
[iconv]: https://en.wikipedia.org/wiki/Iconv
