// Copyright 2023 Daniel Erat.
// All rights reserved.

package main

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"testing"
)

func TestTranslateSubtitles(t *testing.T) {
	const (
		head1 = "1\n00:01:20,800 --> 00:01:24,600\n"
		en1   = "Everybody, heads up!\nHeads up! Keep clear!"
		zh1   = "大家，抬起头来！\n小心！保持清醒！"

		head2 = "2\n00:01:24,700 --> 00:01:27,800\n"
		en2   = "Keep it clear!\nOkay, down!"
		zh2   = "保持清晰！\n好吧，下来！"

		// Full input and output.
		input  = head1 + en1 + "\n\n" + head2 + en2 + "\n"
		output = head1 + zh1 + "\n" + en1 + "\n\n" + head2 + zh2 + "\n" + en2 + "\n"
	)

	for _, tc := range []struct {
		name  string
		in    string
		trans map[string]string
		max   int
		opts  options
		want  string
	}{
		{
			name:  "full translation",
			in:    input,
			trans: map[string]string{en1 + "\n" + en2: zh1 + "\n" + zh2},
			max:   120,
			want:  output,
		},
		{
			name:  "split translation",
			in:    input,
			trans: map[string]string{en1: zh1, en2: zh2},
			max:   45,
			want:  output,
		},
		{
			name:  "max lines",
			in:    input,
			trans: map[string]string{en1: zh1},
			opts:  options{maxLines: 2},
			want:  head1 + zh1 + "\n" + en1 + "\n\n" + head2 + en2 + "\n",
		},
		{
			name:  "leading zero-width no-break space",
			in:    "\uFEFF" + input,
			trans: map[string]string{en1 + "\n" + en2: zh1 + "\n" + zh2},
			want:  output,
		},
		{
			name:  "latin1 input",
			in:    head1 + "Ma\xf1ana\n", // Mañana
			trans: map[string]string{"Mañana": "Tomorrow"},
			opts:  options{srcCharset: "latin1"},
			want:  head1 + "Tomorrow\nMañana\n",
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.Background()
			r := strings.NewReader(tc.in)
			var b bytes.Buffer
			tr := fakeTranslator{translations: tc.trans, mr: tc.max}
			if err := translateSubtitles(ctx, r, &b, &tr, &tc.opts); err != nil {
				t.Error("translateSubtitles failed:", err)
			} else if got := b.String(); got != tc.want {
				t.Errorf("translateSubtitles returned:\n%v\nwant:\n%v", got, tc.want)
			}
		})
	}
}

type fakeTranslator struct {
	translations map[string]string
	mr, ml       int // max runes and lines
}

func (tr *fakeTranslator) translate(ctx context.Context, orig []string) ([]string, error) {
	if out, ok := tr.translations[strings.Join(orig, "\n")]; ok {
		return strings.Split(out, "\n"), nil
	}
	return nil, fmt.Errorf("no translation for %q", orig)
}

func (t *fakeTranslator) maxRunes() int { return t.mr }
func (t *fakeTranslator) maxLines() int { return t.ml }
