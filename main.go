// Copyright 2023 Daniel Erat.
// All rights reserved.

package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"

	"golang.org/x/text/encoding/ianaindex"
	"golang.org/x/text/transform"

	translate "cloud.google.com/go/translate/apiv3"
	translatepb "cloud.google.com/go/translate/apiv3/translatepb"
)

func main() {
	var tr gcpTranslator
	var opts options

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [flag]... <in.srt> <out.srt>\n"+
			"Translate a SubRip .srt subtitle file using GCP.\n"+
			"Reads from stdin and writes to stdout if no args are supplied.\n\n",
			os.Args[0])
		flag.PrintDefaults()
	}
	flag.StringVar(&opts.srcCharset, "src-charset", "", "Source charset if not ASCII or UTF-8, e.g. \"latin1\"")
	flag.StringVar(&tr.srcLang, "src-lang", "en-US", "Source language")
	flag.StringVar(&tr.dstLang, "dst-lang", "zh-CN", "Target language")
	flag.StringVar(&tr.projectID, "project-id", os.Getenv("TRANSLATE_SUBTITLES_PROJECT_ID"), "GCP project ID")
	flag.IntVar(&opts.maxLines, "max-lines", 0, "Maximum number of lines to translate")
	flag.Parse()

	var r io.Reader
	var w io.Writer
	switch len(flag.Args()) {
	case 0:
		r, w = os.Stdin, os.Stdout
	case 2:
		in, err := os.Open(flag.Arg(0))
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(2)
		}
		defer in.Close()

		out, err := os.Create(flag.Arg(1))
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		defer func() {
			if err := out.Close(); err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}()

		r, w = in, out
	default:
		flag.Usage()
		os.Exit(2)
	}

	if err := translateSubtitles(context.Background(), r, w, &tr, &opts); err != nil {
		fmt.Fprintln(os.Stderr, "Translating subtitles failed:", err)
		os.Exit(1)
	}
	os.Exit(0)
}

type options struct {
	maxLines   int    // maximum number of lines of text to translate
	srcCharset string // e.g. "ISO-8859-1" or "latin1"
}

// translateSubtitles reads an SRT file from r, translates it using tr,
// and writes the resulting data to w.
func translateSubtitles(ctx context.Context, r io.Reader, w io.Writer, tr translator, opts *options) error {
	// Transform the input if requested.
	if opts.srcCharset != "" {
		enc, err := ianaindex.MIME.Encoding(opts.srcCharset)
		if err != nil {
			return fmt.Errorf("bad charset %q: %v", opts.srcCharset, err)
		}
		r = transform.NewReader(r, enc.NewDecoder())
	}

	// Read the subtitle data.
	var lines []string
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		// I've seen at least one SRT file that contained a zero-width no-break space (U+FEFF)
		// at the beginning of one of its counter lines. strings.TrimSpace doesn't remove these
		// (https://github.com/golang/go/issues/42274), so strip them and zero-width space (U+200B)
		// manually.
		ln := strings.TrimSpace(sc.Text())
		ln = strings.Trim(ln, "\u200B\uFEFF")
		lines = append(lines, ln)
	}
	if err := sc.Err(); err != nil {
		return fmt.Errorf("reading input: %v", err)
	}

	// Identify the text lines so we can translate only them (since Cloud Translation seems
	// annoyingly overpriced, and we don't want it messing with SRT counter numbers or
	// timespans anyway).
	var orig, trans []string       // lines of original and translated text
	insertIdx := -1                // current lines index to insert translated lines at
	inserts := make(map[int][]int) // lines index -> trans indexes to insert there
	for i, ln := range lines {
		switch {
		case ln == "":
			insertIdx = -1
		case insertIdx >= 0:
			orig = append(orig, ln)
			inserts[insertIdx] = append(inserts[insertIdx], len(orig)-1)
		case timeRegexp.MatchString(ln):
			insertIdx = i + 1
		}
	}

	// Limit the number of lines to translate if requested.
	if opts.maxLines > 0 && len(orig) > opts.maxLines {
		log.Printf("Truncating input to %d line(s)", opts.maxLines)
		orig = orig[:opts.maxLines]
	}

	// Actually perform the translation.
	log.Printf("Translating %d lines", len(orig))

	var buf []string // in-progress text to send
	var bufRunes int // runes in buf
	sendBuf := func() error {
		log.Printf("Sending %v lines and %v code points", len(buf), bufRunes)
		out, err := tr.translate(ctx, buf)
		if err != nil {
			return err
		}
		var outRunes int
		for _, ln := range out {
			outRunes += len([]rune(ln))
		}
		log.Printf("Received %v lines and %v code points", len(out), outRunes)
		trans = append(trans, out...)
		buf = nil
		bufRunes = 0
		return nil
	}

	for _, ln := range orig {
		// Send a request if we'd go beyond the translator's per-request rune or line limit.
		nr := len([]rune(ln))
		if (tr.maxRunes() > 0 && bufRunes+nr > tr.maxRunes()) ||
			(tr.maxLines() > 0 && len(buf)+1 >= tr.maxLines()) {
			if err := sendBuf(); err != nil {
				return err
			}
		}
		buf = append(buf, ln)
		bufRunes += nr
	}
	if len(buf) > 0 {
		if err := sendBuf(); err != nil {
			return err
		}
	}

	if len(trans) != len(orig) {
		return fmt.Errorf("sent %d lines but translation contains %d", len(orig), len(trans))
	}

	// Write the original lines, inserting the translated ones when needed.
	for i, ln := range lines {
		for _, j := range inserts[i] {
			if j < len(trans) {
				if _, err := fmt.Fprintln(w, trans[j]); err != nil {
					return fmt.Errorf("writing output: %v", err)
				}
			}
		}
		if _, err := fmt.Fprintln(w, ln); err != nil {
			return fmt.Errorf("writing output: %v", err)
		}
	}
	return nil
}

// timeRegexp matches a timespan line in an SRT file, e.g. "00:00:31,331 --> 00:00:33,197".
var timeRegexp = regexp.MustCompile(`^\d\d:\d\d:\d\d,\d\d\d --> \d\d:\d\d:\d\d,\d\d\d$`)

// translator translates text from one language to another.
type translator interface {
	// translate translates the supplied text.
	translate(ctx context.Context, orig []string) ([]string, error)
	// maxRunes returns the maximum number of runes that can be passed to translate.
	maxRunes() int
	// maxLines returns the maximum number of lines that can be passed to translate.
	maxLines() int
}

// gcpTranslator implements translator using the Google Cloud Translate API.
type gcpTranslator struct{ projectID, srcLang, dstLang string }

func (tr *gcpTranslator) translate(ctx context.Context, orig []string) ([]string, error) {
	cl, err := translate.NewTranslationClient(ctx)
	if err != nil {
		return nil, err
	}
	defer cl.Close()

	resp, err := cl.TranslateText(ctx, &translatepb.TranslateTextRequest{
		Parent:             fmt.Sprintf("projects/%s/locations/global", tr.projectID),
		Contents:           orig,
		MimeType:           "text/plain", // defaults to "text/html"
		SourceLanguageCode: tr.srcLang,
		TargetLanguageCode: tr.dstLang,
	})
	if err != nil {
		return nil, err
	}
	lines := make([]string, len(resp.Translations))
	for i, t := range resp.Translations {
		lines[i] = t.TranslatedText
	}
	return lines, nil
}

// https://cloud.google.com/translate/quotas#content_limit_per_request:
// "For Cloud Translation - Advanced, the maximum number of code points for
// a single request is 30K."
func (t *gcpTranslator) maxRunes() int { return 30_000 }

// https://pkg.go.dev/cloud.google.com/go/translate@v1.12.2/apiv3/translatepb#TranslateTextRequest:
// "The max length of [the Contents] field is 1024."
func (t *gcpTranslator) maxLines() int { return 1024 }
